<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Store</title>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css"/>
</head>
<body>
	<!-- HEADER -->
	<header class="header bg-info">
		<div class="container d-flex align-items-center">
			<a href="" class="logo d-block">
				<img class="w-100" src="assets/images/logo-x.png" alt="">
			</a>
			<nav class="main-menu ml-auto mr-0">
				<div class="menu-content d-flex flex-column position-relative">
					<div class="top d-flex flex-row align-items-center justify-content-around">
                                            <button class="btn btn-login">Iniciar Sesión</button>
                                            <button class="btn btn-signup">Registrarse</button>
					</div>
					<ul class="d-flex p-0 flex-column w-100 items">
                                            <li>
                                                <a href="#">Libros</a>
                                                <div class="submenu-content">
                                                    <div class="submenu-header d-flex align-items-center">
                                                        <button class="goback"><i class="fa fa-fw fa-angle-left fa-2x"></i></button>
                                                        <h5>Libros</h5>
                                                    </div>
                                                    <ul class="p-0 m-0">
                                                        <li><a href="#">Matemáticas</a></li>
                                                        <li><a href="#">Física</a></li>
                                                        <li><a href="#">Química</a></li>
                                                        <li><a href="#">Ingenieria</a></li>
                                                        <li><a href="#">Derecho</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li><a href="#">Arquitectura</a></li>
                                            <li><a href="#">Ingenieria</a></li>
                                            <li><a href="#">Oficina</a></li>
                                            <li><a href="#">Utiles</a></li>
					</ul>
				</div>
			</nav>
			<button class="ml-auto main-menu-btn">
				<label></label>
				<label></label>
				<label></label>
			</button>
		</div>
	</header>
        <section>
            
        </section>
        <footer>
            
        </footer>
	<script type="text/javascript" src="assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery-ui-touch-punch.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/main.js"></script>
	<script type="text/javascript" src="assets/js/main_menu.js"></script>
</body>
</html>